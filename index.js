$("body").on("click", ".primary", function() {
  $(this).parent().next().toggle();
  $(this).parent().css({
    "border-bottom": "1px solid #ccc",
    "border-radius": "5px"
  });
});

let limit = 10;

function hidePackageLock() {
  const domEls = $('.commentable-diff[data-path*="package-lock.json"]').children().children().children().click();
  if( (!domEls || !domEls.length) && limit > 0 ) {
    limit--;
    setTimeout( hidePackageLock, 1000 );
  }
}

hidePackageLock();